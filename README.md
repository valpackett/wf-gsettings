# wf-gsettings

GSettings plugin for [Wayfire].

- install latest Wayfire, GLib, `xsltproc` (from `libxslt`)
- install this (regular Meson build)
- (for both Wayfire and this, use a prefix that gets read by GSettings!)
- run `wf-gsettings-gen-schemas`
	- rerun when updating Wayfire/plugins
	- it generates GSettings schemas from Wayfire plugin metadata
		- schemas are named `org.wayfire.plugin.<name>`
	- and runs `glib-compile-schemas` for you
- import your settings into dconf, if you want (the ini file won't be read at all!)
- start Wayfire with new args: `-B PREFIX/lib/wayfire/libgsettings.so`
	- (PREFIX being `/usr/local`, `/usr`, `$HOME/.local`… wherever you've installed it)
	- also make sure you're in a D-Bus session when starting Wayfire! You should already be if using logind. Otherwise start Wayfire via `dbus-run-session`.
- play around with [dconf-editor]

[Wayfire]: https://github.com/WayfireWM/wayfire
[dconf-editor]: https://wiki.gnome.org/Apps/DconfEditor

## Editing settings via CLI

Read:

```shell
% dconf dump /org/wayfire
[gsettings]
dyn-sections=['output:DP-1', 'wallpaper:w1']

[section/output/DP-1]
scale=2.0

[section/wallpaper/w1]
path='/usr/local/share/backgrounds/gnome/adwaita-day.jpg'

[section/blur]
mode='toggle'
toggle='<alt> BTN_MIDDLE'

[section/command]
bindings={'term': ('alacritty', '<super> KEY_ENTER')}
repeatable-bindings={'vup': ('pactl set-sink-volume @DEFAULT_SINK@ +2%', 'KEY_VOLUMEUP'), 'vdn': ('pactl set-sink-volume @DEFAULT_SINK@ -2%', 'KEY_VOLUMEDOWN')}
…
```

You can save that into a file and then restore with `dconf load /org/wayfire`.

Also, note the `/org/wayfire/gsettings/dyn-sections` key in the example.
You must put the names of dynamic sections (like outputs) there.

Write (you only need `dbus-launch` if outside the dbus session):

```shell
% dbus-launch dconf write /org/wayfire/section/core/plugins "'animate annotate mag cube expo resize command oswitch grid window-rules autostart wrot place invert move switcher fast-switcher  alpha mod2key idle vswitch scale  vswipe wm-actions showrepaint wallpaper'"
```

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).
